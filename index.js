/*თვქენი დავალებაა გააკეთოთ character  
ების კლასი სადაც შეინახავთ შემდეგ ინფორმაციას:
სახელი და როლი (როლი შეიძლება იყოს რამოდენიმენაირი:
mage, support, assassin, adc, tank (არ არის აუცილებელი ამ
ჩამონათვლის სადმე შენახვა ან ყველა როლის გამოყენება)პ0ო, 
აქედან support mage  - ც არის. ყველა პერსონაჟს სახელთან 
და როლთან ერთად აქვს armor ისა და damage ის მეთოდები,
რომლებიც უბრალოდ ლოგავენ თუ რამდენი არმორი აქვს და 
რამდენი dmg აქვს პერსონაჟს. damage ის შემთხვევაში შეგვიძლია
გვააკეთოთ შემოწმება mage, support ზე და სხვა როლებზე, 
პირველი ორის შემთხვევაში პერსონაჟი magic damage ს აკეთებს,
ხოლო სხვა დანარჩენ შემთვევებში attack damage და მეთოდიც 
ამას ლოგავს.  mage ის კლასი ასევე მიიღებს magicPowers და 
აქვს ფრენის მეთოდიც (mage -ები დაფრინავენ კიდეც), რომელიც 
დალოგავს, რომ ამ მეიჯს შეუძლია ფრენაც. support იგივე მეიჯია, 
რომელიც მიიღებს heals - ს (თუ რამდენი "ერთეულის" დაჰილვა შეუძლია) 
ცვლადად და ექნნება  healing მეთოდი რომელიც დალოგავს, რომ ამ გმირს 
შეუძლია გარკვეული რაოდენობის ერთეულის დაჰილვა. ასევე გვყავს adc 
რომელიც მიიღებს attackdamages ს და აქვს მეთოდი, რომელიც ითვლის მის 
რეინჯს (მეთოდმა რეინჯის მნიშვნელობა უნდა მიიღოს), ამაზე დაყრდნობით 
ის ლოგავს ახლო რეინჯიანია პერსონაჟი თუ არა. (პირობითად ვთქვათ, რომ 
30 < ახლო რეინჯში ითვლება)


რაც შეეხება თვითონ პერსონაჟების სახელებს და ყველა იმ მნიშვნელობებს 
რომლებიც კლასებს უნდა გადასცეთ its up to you. თქვენი ამოცანა კლასების 
შექმნაში და მათი სტრუქტურის სწორ აგებულებაში მდგომარეობს. */

class Charecter{
    name = '';
    role = '';
    constructor(name, role) {
        this.name = name;
        this.role = role;
    }
    armor(arm){
        console.log(`Armor ${arm}`);
    }
    damage(dmg){
        console.log(`${this.name} Have Damage: ${dmg}`);
        console.log(`${this.name} Have Attack Damage`);
    }
}

//----------------Mage------------\\
class Mage extends Charecter{
    magicPowers = '';
    constructor(name, role, magicPowers){
        super(name, role, magicPowers);
        this.magicPowers = magicPowers;
    }
    fly(){
        console.log(`${this.name} can fly`)
    }
    damage(dmg){
        console.log(`${this.name} Have Damage: ${dmg}`);
        console.log(`${this.name} Have Magic damage`);
    }
}

//----------------Support--------------\\
class Support extends Mage{
    constructor(name, role, magicPowers){
        super(name, role, magicPowers);
    }
    healer(heals){
        console.log(`Support can heal ${heals} people`);
    }
}

//-----------------ADC---------------\\
class Adc extends Charecter{
    attackdamages = '';
    constructor(name, attackdamages){
        super(name, 'Adc', attackdamages)
        this.attackdamages = attackdamages;
    }
    range(rang){
        if(rang < 30){
            console.log(`${this.name} is short range charecter. range:${rang}`);
        }else{
            console.log(`${this.name} is long range charecter. range:${rang}`);
        }
    }
}

let magic = new Mage('Gendalf', 'Mage', 'Flyes');
let supporter = new Support('Arthour', 'Support', 'Heals')
let adcer = new Adc('Raider', 30);

//Mage
magic.fly();
magic.damage(125);
magic.armor(1000);
console.log(magic);

//Support
supporter.damage(150);
supporter.armor(900);
supporter.healer(20);
console.log(supporter);

//Adc
adcer.damage(100);
adcer.armor(1500);
adcer.range(35);
console.log(adcer);